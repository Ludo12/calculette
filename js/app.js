$(document).ready(function () {
    // affichage
    let input = document.createElement("input");
    input.type = "text";
    input.id = "id_input";
    input.value = "";
    input.disabled = true;
    document.body.appendChild(input);

    let section = document.createElement("section");
    section.id = "debut";
    document.body.appendChild(section);

    let index;
    for (index = 0; index < 2; index++) {
        let section = document.createElement("section");
        section.id = "section_" + index;
        document.getElementById("debut").appendChild(section);
    }

    function div(valeur, ident) {
        for (index = 0; index < 5; index++) {
            let div = document.createElement("div");
            div.id = ident + index;
            div.className = "test";
            document.getElementById(valeur).appendChild(div);
        }
    }
    div("section_0", "divSect0_");
    div("section_1", "divSect1_");



    for (index = 0; index < 10; index++) {
        let button = document.createElement("button");
        button.innerHTML = index;
        button.id = "id_" + index;
        button.value = index;
        if (index == 0) {
            document.getElementById("divSect0_4").appendChild(button);
        } else if (index > 0 && index < 4) {
            document.getElementById("divSect0_3").appendChild(button);
        } else if (index >= 4 && index < 7) {
            document.getElementById("divSect0_2").appendChild(button);
        } else {
            document.getElementById("divSect0_1").appendChild(button);
        }
    }

    //Positionnement de +,-,*,/,CE et C


    let plus = document.createElement("button");
    plus.innerHTML = "+";
    plus.id = "plus";
    plus.value = "+";
    document.getElementById("divSect1_2").appendChild(plus);

    let moins = document.createElement("button");
    moins.innerHTML = "-";
    moins.id = "moins";
    moins.value = "-";
    document.getElementById("divSect1_1").appendChild(moins);

    let multi = document.createElement("button");
    multi.innerHTML = "x";
    multi.id = "multi";
    multi.value = "x";
    document.getElementById("divSect1_2").appendChild(multi);

    let diviser = document.createElement("button");
    diviser.innerHTML = "/";
    diviser.id = "diviser";
    diviser.value = "/";
    document.getElementById("divSect1_1").appendChild(diviser);

    let point = document.createElement("button");
    point.innerHTML = ".";
    point.id = "point";
    point.value = ".";
    document.getElementById("divSect0_4").appendChild(point);

    let egal = document.createElement("button");
    egal.innerHTML = "=";
    egal.id = "egal";
    egal.value = "=";
    document.getElementById("divSect1_4").appendChild(egal);

    let ce = document.createElement("button");
    ce.innerHTML = "CE";
    ce.id = "ce";
    ce.value = "ce";
    document.getElementById("divSect1_0").appendChild(ce);

    let c = document.createElement("button");
    c.innerHTML = "C";
    c.id = "c";
    c.value = "c";
    document.getElementById("divSect1_0").appendChild(c);



    let chiffre = id_input.value;
    let myElement = document.getElementById("id_input");
    $("#id_0").click(function () {
        chiffre += id_0.value;
        myElement.value = parseFloat(chiffre);
        return myElement;
    });
    $("#id_1").click(function () {
        chiffre += id_1.value;
        myElement.value = parseFloat(chiffre);
        return myElement;
    });
    $("#id_2").click(function () {
        chiffre += id_2.value;
        myElement.value = parseFloat(chiffre);
        return myElement;
    });
    $("#id_3").click(function () {
        chiffre += id_3.value;
        myElement.value = parseFloat(chiffre);
        return myElement;
    });
    $("#id_4").click(function () {
        chiffre += id_4.value;
        myElement.value = parseFloat(chiffre);
        return myElement;
    });
    $("#id_5").click(function () {
        chiffre += id_5.value;
        myElement.value = parseFloat(chiffre);
        return myElement;
    });
    $("#id_6").click(function () {
        chiffre += id_6.value;
        myElement.value = parseFloat(chiffre);
        return myElement;
    });
    $("#id_7").click(function () {
        chiffre += id_7.value;
        myElement.value = parseFloat(chiffre);
        return myElement;
    });
    $("#id_8").click(function () {
        chiffre += id_8.value;
        myElement.value = parseFloat(chiffre);
        return myElement;
    });
    $("#id_9").click(function () {
        chiffre += id_9.value;
        myElement.value = parseFloat(chiffre);
        return myElement;
    });
    $("#point").click(function () {
        chiffre += point.value;
        myElement.value = parseFloat(chiffre);
        return myElement;
    });

    // calculette
    let res = "";
    let boolp = false;
    let boolm = false;
    let boolmu = false;
    let boold = false;
    $("#plus").click(function () {
        if ("" == res) {
            res = parseFloat(chiffre);
        } else {
            res += parseFloat(chiffre);
        }
        chiffre = 0;
        myElement.value = "";
        boolp = true;
        return res;
    });
    $("#moins").click(function () {
        if ("" == res) {
            res = parseFloat(chiffre);
        } else {
            chiffre = myElement.value;
            res -= parseFloat(chiffre);
        }
        chiffre = 0;
        myElement.value = "";
        boolm = true;
        return res;
    });
    $("#multi").click(function () {
        if ("" == res) {
            res = parseFloat(chiffre);
        } else {
            res *= parseFloat(chiffre);
        }
        chiffre = 0;
        myElement.value = "";
        boolmu = true;
        return res;
    });
    $("#diviser").click(function () {
        if ("" == res) {
            res = parseFloat(chiffre);
        } else {
            res = res / parseFloat(chiffre);
        }
        chiffre = 0;
        myElement.value = "";
        boold = true;
        return res;
    });
    $("#ce").click(function () {
        myElement.value = 0;
        chiffre = 0;
        return res;
    });
    $("#c").click(function () {
        chiffre = 0;
        res = 0;
        boolp = false;
        boolm = false;
        boolmu = false;
        boold = false;
        myElement.value = chiffre;
        return chiffre;
    });
    $("#egal").click(function () {
        if (true == boolp) {
            res += parseFloat(chiffre);
            myElement.value = parseFloat(res);
        }
        if (true == boolm) {
            res -= parseFloat(chiffre);
            myElement.value = parseFloat(res);
        }
        if (true == boolmu) {
            res *= parseFloat(chiffre);
            myElement.value = parseFloat(res);
        }
        if (true == boold) {
            if (0 == chiffre) {
                alert('Erreur !! On ne peut pas diviser par 0.');
            } else {
                res = res / parseFloat(chiffre);
                myElement.value = parseFloat(res);
            }
        }
        return myElement;
    });
});